unit JsonUtil;

interface

uses
  REST.Response.Adapter, Data.DB,System.SysUtils,
  Generics.Collections, Rest.Json,System.JSON, FireDAC.Comp.Client,
  Helper;

procedure Json2Dataset(aDataset : TDataSet; aJSON : string);
function Dataset2Json(aDataset : TFDMemTable):String;

implementation



procedure Json2Dataset(aDataset : TDataSet; aJSON : string);
var
  JObj: TJSONArray;
  vConv : TCustomJSONDataSetAdapter;
begin
  if (aJSON = EmptyStr) then
  begin
    Exit;
  end;

  JObj := TJSONObject.ParseJSONValue(aJSON) as TJSONArray;
  vConv := TCustomJSONDataSetAdapter.Create(Nil);

  try
    vConv.Dataset := aDataset;
    vConv.UpdateDataSet(JObj);
  finally
    vConv.Free;
    JObj.Free;
  end;
end;

function Dataset2Json(aDataset : TFDMemTable):String;
begin
  Result := aDataset.ToJson;
end;

end.
