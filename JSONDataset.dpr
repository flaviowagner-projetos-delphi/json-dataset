program JSONDataset;

uses
  Vcl.Forms,
  UntMain in 'UntMain.pas' {FrmMain},
  JsonUtil in 'JsonUtil.pas',
  Helper in 'Helper.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmMain, FrmMain);
  Application.Run;
end.
