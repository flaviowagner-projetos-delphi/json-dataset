object FrmMain: TFrmMain
  Left = 0
  Top = 0
  Caption = 'JSON2Dataset / Dataset2JSON'
  ClientHeight = 272
  ClientWidth = 674
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 228
    Top = 0
    Width = 5
    Height = 272
    ExplicitHeight = 460
  end
  object GroupBox1: TGroupBox
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 222
    Height = 266
    Align = alLeft
    Caption = '  JSON  '
    TabOrder = 0
    ExplicitHeight = 431
    object MemoJSON: TMemo
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 212
      Height = 174
      Align = alClient
      Lines.Strings = (
        '[{"nome":"Flavio '
        'Wagner","idade":"36","profissao":"Desen'
        'volvedor","genero":"Masculino"},'
        '{"nome":"Jos'#233'","idade":"40","profissao":"'
        'DBA","genero":"Masculino"},'
        '{"nome":"Maria","idade":"29","profissao":'
        '"Analista '
        'Requisitos","genero":"Feminino"}]')
      TabOrder = 0
      ExplicitLeft = 7
      ExplicitTop = 15
      ExplicitHeight = 323
    end
    object Panel1: TPanel
      Left = 2
      Top = 195
      Width = 218
      Height = 69
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitTop = 360
      object Button1: TButton
        AlignWithMargins = True
        Left = 3
        Top = 37
        Width = 212
        Height = 29
        Align = alBottom
        Caption = 'Dataset 2 JSON'
        TabOrder = 0
        OnClick = Button1Click
        ExplicitLeft = 0
        ExplicitTop = 56
        ExplicitWidth = 218
      end
      object Button2: TButton
        AlignWithMargins = True
        Left = 3
        Top = 2
        Width = 212
        Height = 29
        Align = alBottom
        Caption = 'JSON 2 DataSet'
        TabOrder = 1
        OnClick = Button2Click
        ExplicitLeft = 0
        ExplicitTop = 56
        ExplicitWidth = 218
      end
    end
  end
  object GroupBox2: TGroupBox
    AlignWithMargins = True
    Left = 236
    Top = 3
    Width = 435
    Height = 266
    Align = alClient
    Caption = '  Datdos  '
    TabOrder = 1
    ExplicitLeft = 328
    ExplicitTop = 136
    ExplicitWidth = 185
    ExplicitHeight = 105
    object DBNavigator1: TDBNavigator
      AlignWithMargins = True
      Left = 5
      Top = 18
      Width = 425
      Height = 47
      DataSource = DataSource
      Align = alTop
      TabOrder = 0
      ExplicitLeft = 7
    end
    object DBGrid1: TDBGrid
      AlignWithMargins = True
      Left = 5
      Top = 71
      Width = 425
      Height = 190
      Align = alClient
      DataSource = DataSource
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
    end
  end
  object DataSource: TDataSource
    DataSet = FDMemTable
    Left = 356
    Top = 203
  end
  object FDMemTable: TFDMemTable
    FetchOptions.AssignedValues = [evMode]
    FetchOptions.Mode = fmAll
    ResourceOptions.AssignedValues = [rvSilentMode]
    ResourceOptions.SilentMode = True
    UpdateOptions.AssignedValues = [uvCheckRequired, uvAutoCommitUpdates]
    UpdateOptions.CheckRequired = False
    UpdateOptions.AutoCommitUpdates = True
    Left = 356
    Top = 155
  end
end
